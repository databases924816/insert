-- 1: Insert the film
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('The Godfather', 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 1972, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 175, 19.99, 'R', NOW());

-- 2: Insert actors into the "actor" table
INSERT INTO actor (first_name, last_name, last_update)
VALUES ('Al', 'Pacino', NOW()),
       ('John', 'Travolta', NOW()),
       ('Heath', 'Ledger', NOW());

-- 3: Link actors to the film in the "film_actor" table
INSERT INTO film_actor (actor_id, film_id, last_update)
VALUES ((SELECT actor_id FROM actor WHERE first_name = 'Al' AND last_name = 'Pacino'), (SELECT film_id FROM film WHERE title = 'The Godfather'), NOW()),
       ((SELECT actor_id FROM actor WHERE first_name = 'John' AND last_name = 'Travolta'), (SELECT film_id FROM film WHERE title = 'The Godfather'), NOW()),
       ((SELECT actor_id FROM actor WHERE first_name = 'Heath' AND last_name = 'Ledger'), (SELECT film_id FROM film WHERE title = 'The Godfather'), NOW());

-- 4: Insert 2 more films
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('Pulp Fiction', 'The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.', 1994, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 154, 19.99, 'R', NOW());

INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('The Dark Knight', 'When the menace known as the Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham.', 2008, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 152, 19.99, 'PG-13', NOW());

-- 5: Insert all films into inventory for store 1
INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'The Godfather'), 1, NOW());

INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'Pulp Fiction'), 1, NOW());

INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'The Dark Knight'), 1, NOW());
